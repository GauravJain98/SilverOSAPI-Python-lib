import silverosapi
v = 1
while v < 2:
	c1 = input("Ok, ora inserisci la prima città: ")
	m1 = silverosapi.Meteo(c1)
	if m1.Longitudine() is None:
		print("Città non trovata")
	else:
		v = 2

while v < 3:
	c2 = input("Ok, ora inserisci la seconda città: ")
	m2 = silverosapi.Meteo(c2)
	if m2.Longitudine() is None:
		print("Città non trovata")
	else:
		v = 3

print("Calcolando la distanza...")
distanza = silverosapi.distanza(m1.Latitudine(), m1.Longitudine(), m2.Latitudine(), m2.Longitudine())
if distanza is None:
	print("Non ho trovato alcuna strada che congiunge queste due città")
else:
	print(distanza)
