import requests
import json
def sr(url, args = 0):
	r = requests.get(url, params=args)
	return r.text
class InstagramPost:
	def __init__(self,post):
		self.post = post
	def getCaption(self):
		if "caption" in self.post:
			return self.post["caption"]
		else:
			return 0
	def getTime(self):
		if "time" in self.post:
			return self.post["time"]
		else:
			return 0
	def getLikes(self):
		if "likes" in self.post:
			return self.post["likes"]
		else:
			return 0
	def getComments(self):
		if "comments" in self.post:
			return self.post["comments"]
		else:
			return 0
	def isVideo(self):
		if "is_video" in self.post:
			return self.post["is_video"]
		else:
			return 0
	def getImageUrl(self):
		if "url" in self.post:
			return self.post["url"]
		else:
			return 0
class instagram:
	def __init__(self,username):
		self.res = sr("http://api.silveros.it/instagram.php", {"username" : username})
		self.j = json.loads(self.res)
	def getName(self):
		if "name" in self.j:
			return self.j["name"]
		else:
			return 0
	def getBio(self):
		if "bio" in self.j:
			return self.j["bio"]
		else:
			return 0
	def getPostsCount(self):
		if "posts_count" in self.j:
			return self.j["posts_count"]
		else:
			return 0
	def getFollowers(self):
		if "followers" in self.j:
			return self.j["followers"]
		else:
			return 0
	def getFollowing(self):
		if "following" in self.j:
			return self.j["following"]
		else:
			return 0
	def getPropicUrl(self):
		if "propic" in self.j:
			return self.j["propic"]
		else:
			return 0
	def getPosts(self):
		if "posts" in self.j:
			return self.j["posts"]
		else:
			return 0
class youtube:
	def __init__(self,channel):
		self.res = sr("http://api.silveros.it/yt.php", {"youtuber" : channel})
		self.j = json.loads(self.res)
	def Name(self):
		if "name" in self.j:
			return self.j["name"]
		else:
			return 0
	def Subscribers(self):
		if "subscribers" in self.j:
			return self.j["subscribers"]
		else:
			return 0
	def LastVideo(self):
		if "lastvideo" in self.j:
			return self.j["lastvideo"]
		else:
			return 0
	def Views(self):
		if "views" in self.j:
			return self.j["views"]
		else:
			return 0
class Meteo:
	def __init__(self,city):
		self.res = sr("http://api.silveros.it/meteo.php", {"city" : city})
		self.j = json.loads(self.res)
	def Latitudine(self):
		if "latitudine" in self.j:
			return self.j["latitudine"]
		else:
			return 0
	def Longitudine(self):
		if "longitudine" in self.j:
			return self.j["longitudine"]
		else:
			return 0
	def Umidita(self):
		if "umidita" in self.j:
			return self.j["umidita"]
		else:
			return 0
	def Pressione(self):
		if "pressione" in self.j:
			return self.j["pressione"]
		else:
			return 0
	def Meteo(self):
		if "meteo" in self.j:
			return self.j["meteo"]
		else:
			return 0
	def Vento(self):
		if "vento" in self.j:
			return self.j["vento"]
		else:
			return 0
class palinsesto:
	def __init__(self) :
		self.res = sr("http://api.silveros.it/tv.php")
		self.j = json.loads(self.res)
	def Channels (self):
		list = []
		for x in self.j:
			if x == "testo":
				pass
			else:
				list.append(x)
		return list
	def Program(self,channel) :
		if channel in self.j:
			return self.j[channel]["programma"]
		else:
			return 0
	def getTime(self,channel) :
		if channel in self.j:
			return self.j[channel]["ora"]
		else:
			return 0
class bestemmiometro:
	def __init__(self, chatID):
		self.chatID = chatID
		self.res = sr("http://api.silveros.it/bestemmiometro.php", {"chat_id" : chatID})
		self.j = json.loads(self.res)
		
		
	def Name(self):
		if "result" in self.j:
			return self.j["result"]["nome"]
		else:
			return 0
	def Bestemmie(self):
		if "result" in self.j:
			return self.j["result"]["bestemmie"]
		else:
			return 0
	def PicUrl(self):
		if "result" in self.j:
			return self.j["result"]["pic"]
		else:
			return 0

def distanza (latitudine1,longitudine1,latitudine2,longitudine2):
	return sr("http://api.silveros.it/distanza.php", {"longitudine1" : longitudine1,"latitudine1":latitudine1,"longitudine2":longitudine2,"latitudine2":latitudine2})