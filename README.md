# SilverOSAPI-Python-lib
Questo codice dovrebbe aiudare all'uso di [api.silveros.it](http://api.silveros.it) in python
- - -
# Implementazione
Scaricate silverosapi.py e spostatelo nella cartella principale dello script.
Per implementare il codice nel vostro vi basterà scrivere ```import silverosapi``` all'inizio o nel file dedicato alle funzioni.
Potete trovare gli esempi nella cartella esempi
Dipendenze: `requests
json`
